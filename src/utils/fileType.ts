import FileType, { FileTypeResult } from 'file-type'

/**
 * Using the file-type module slice the Bufffer for first 100 bytes and try to detect the file type with mime
 * @param content
 * @returns FileTypeResult | undefined
 */
export default async function fileType(content: Buffer): Promise<FileTypeResult | undefined> {
  return await FileType.fromBuffer(content.slice(0, 100))
}
