
/**
 * Async Iterable to buffer.
 * This method will go through the iterable ( for of ) and create the buffer
 * @param iteratable
 * @returns
 */
export default async function itToBuff(iteratable: AsyncIterable<Uint8Array>): Promise<Buffer> {
  let content: Uint8Array[] = []
  for await (const chunk of iteratable) {
    content.push(chunk)
  }

  return Buffer.concat(content)
}
