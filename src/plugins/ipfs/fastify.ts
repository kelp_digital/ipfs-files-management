import { FastifyInstance } from 'fastify'
import fp from 'fastify-plugin'
import ipfsClient, { ipfsClientReturn } from 'ipfs-http-client'

export interface FastifyIpfsOptions {
  endpoint: string
}

declare module 'fastify' {
  export interface FastifyInstance {
    ipfsClient: ipfsClientReturn
  }
}

/**
 * Connect to the IPFS node
 * @param fastify
 * @param options
 */
async function fastifyIpfs(fastify: FastifyInstance, options: any, done: any) {
  const { endpoint } = options
  const ipfs = ipfsClient({
    url: endpoint,
  })
  fastify.decorate('ipfsClient', ipfs)
  done()
}

export default fp(fastifyIpfs, {
  fastify: '^3.0.0',
  name: 'fastify-ipfs',
})
