import f, { FastifyInstance } from 'fastify'
import { IncomingMessage, Server, ServerResponse } from 'http'
import { isEmpty, isNil } from 'ramda'

const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 9876
let ipfsEndpoint = process.env.IPFS_ENDPOINT
if (isEmpty(ipfsEndpoint) || isNil(ipfsEndpoint)) {
  throw new Error('IPFS_ENDPOINT must be set')
} else {
  ipfsEndpoint = process.env.IPFS_ENDPOINT
}

// create the app
const fastify: FastifyInstance<Server, IncomingMessage, ServerResponse> = f({
  logger: true,
})

//////////////////////  assign plugins
fastify.register(import('fastify-sensible'))
fastify.register(import('fastify-healthcheck'))
fastify.register(import('./plugins/ipfs/fastify'), { endpoint: ipfsEndpoint })
fastify.register(import('fastify-multipart'), {
  // attachFieldsToBody: true,
  sharedSchemaId: '#macula',
  limits: {
    fieldNameSize: 100, // Max field name size in bytes
    fieldSize: 1000000, // Max field value size in bytes  1MB
    fields: 10, // Max number of non-file fields
    fileSize: 100 * 1000000, // For multipart forms, the max file size ( 100 is how many megabytes)
    files: 2, // Max number of file fields
    headerPairs: 2000, // Max number of header key=>value pairs
  },
})

fastify.register(require('fastify-swagger'), {
  routePrefix: '/docs',
  swagger: {
    info: {
      title: 'IPFS upload',
      description: 'Kelp IPFS upload API',
      version: '0.1.0',
    },
    host: 'localhost:9876',
    schemes: ['http'],
    consumes: ['application/json'],
    produces: ['application/json'],
    // tags: [
    //   { name: 'user', description: 'User related end-points' },
    //   { name: 'code', description: 'Code related end-points' },
    // ],
  },
  uiConfig: {
    docExpansion: 'full',
    deepLinking: true,
  },
  exposeRoute: true,
})

//////////////////////  assign plugins

//////////////////////  assign routes
fastify.register(import('./routes/ipfs/uploadFile'))
fastify.register(import('./routes/ipfs/readFile'))
//////////////////////  assign routes

/**
 * Home route
 */
fastify.get('/', async (request, reply) => {
  reply.type('application/json').code(200).send({ hello: 'world' })
})

fastify.listen(port, (err, address) => {
  if (err) {
    fastify.log.error(err)
    console.error(err)
    process.exit(1)
  }

  fastify.log.info(`server listening on ${address}`)
})
