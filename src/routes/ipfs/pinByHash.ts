import { FastifyInstance, RouteShorthandOptions } from 'fastify';

export default async function routes(fastify: FastifyInstance) {
  const opts: RouteShorthandOptions = {
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            cid: { type: 'string' },
          }
        }
      }
    },
  };
  fastify.get<{ Params: { cid: string } }>('/ipfs/pinByHash', opts, async (req, reply) => {
    reply.type('application/json').code(200).send({ cid: req.params.cid })
  })

}
