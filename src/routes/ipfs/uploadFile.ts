import { FastifyInstance, RouteShorthandOptions } from 'fastify';
import { createWriteStream } from 'fs';
import { pipeline } from 'stream';
import util from 'util';

const pump = util.promisify(pipeline)

export default async function routes(fastify: FastifyInstance) {
  const opts: RouteShorthandOptions = {
    schema: {
      // response: {
      //   200: {
      //     type: 'object',
      //     properties: {
      //       cid: { type: 'string' },
      //     }
      //   }
      // }
    },

  };
  fastify.post('/ipfs/upload_file', opts, async (req, reply) => {

    if (!req.isMultipart()) {
      reply.code(400).send(new Error('Request is not multipart'))
    }
    const part = await req.file({ limits: { files: 1 } })



    await pump(part.file, createWriteStream(part.filename))
    const uploaded = ({ fileName: part.filename, cid: 'bafy...' })

    reply.type('application/json').code(200).send({ uploaded, body: req.body })
  })

}
