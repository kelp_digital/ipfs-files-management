import { FastifyInstance, RouteShorthandOptions } from 'fastify'
import { isNil } from 'ramda'
import fileType from '../../utils/fileType'
import itToBuff from '../../utils/itToBuff'

export default async function routes(fastify: FastifyInstance) {
  const opts: RouteShorthandOptions = {
    schema: {
      // description: 'post some data',
      // tags: ['user', 'code'],
      // summary: 'qwerty',
      params: {
        type: 'object',
        properties: {
          cid: {
            type: 'string',
            description: 'IPFS content address.',
          },
        },
      },
    },
  }

  /**
   * Read the CID from the IPFS. This api uses `cat` and with the process.env.IPFS_CAT_TIMEOUT = 2000 ms to get the data
   */
  fastify.get<{ Params: { cid: string } }>('/ipfs/cat/:cid', opts, async (req, reply) => {
    try {
      const cid = req.params.cid
      const { ipfsClient } = fastify

      const content = await itToBuff(
        ipfsClient.cat(cid, {
          timeout: process.env.IPFS_CAT_TIMEOUT ? parseInt(process.env.IPFS_CAT_TIMEOUT, 10) : 2000,
        })
      )

      // detect the file type if possible
      const ft = await fileType(content)

      let type = 'plain/text'
      let ret: Buffer | string = ''

      // check did we get the file type
      if (!isNil(ft)) {
        type = ft.mime
        ret = content
      } else {
        fastify.log.info('File type is not found for the CID', cid)
        const str = content.toString()

        // let's try to see is the non media file JSON file
        try {
          // this potentially can be slow
          JSON.parse(str)
          type = 'application/json'
          ret = str
        } catch (error) {
          ret = str
        }
      }

      reply.type(type).code(200).send(ret)
    } catch (error) {
      reply.badRequest(error)
    }
  })
}
